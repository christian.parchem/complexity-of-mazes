Complexity analysis of mazes

Maze solving is often used in psychological studies. For example to study strategies in planning. To do so the difficulty of a maze must be determined. Complexity is one aspect for the difficulty of a maze and therefor important to evaluate a given task on a maze. The goal of this study was to review the various ways of measuring environmental complexity and choose the most suitable ones to examine the complexity of mazes. To find those measures mazes were generated and studied for complexity of the obstacle configuration and for the path complexity. An entropy-compression-based method and a path-complexity-based method were used to study complexity and show its dependence on maze parameters and maze size. 

Files:

create_classic_maze - Create classic mazes and print to maze file, print entropy, compression and complexity to output-file, print maze png

create_random_maze - Create random mazes in 3 types and print to maze file, print entropy, compression and complexity to output-file, print maze png

solve_classic_maze - Solve classic mazes and print path complexity to complexity file

solve_random_maze - Solve random mazes and print path complexity to complexity file

plot_maze_complexity - Plot mean and error of entropy, compression, complexity and path complexity to diagram

size_classic_maze - Create classic mazes for various sizes and print to maze file, print entropy, compression and complexity to output-file, print maze png

size_classic_maze - Create random mazes for various sizes and print to maze file, print entropy, compression and complexity to output-file, print maze png

size_plot_maze_complexity - Plot mean and error of size-dependend entropy, compression, complexity and path complexity to diagram
