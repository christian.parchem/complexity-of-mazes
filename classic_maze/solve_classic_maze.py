# Code from Packt "Artificial Intelligence with Python" 
# https://github.com/PacktPublishing/Artificial-Intelligence-with-Python/blob/master/Chapter%2007/code/maze.py
# Author: Prateek Joshi
# Edited by Christian Parchem
# Date: 6.4.20


import math
import string
from scipy.ndimage import zoom
import numpy as np    
from PIL import Image
from simpleai.search import SearchProblem, astar
from collections import Counter


# Class containing the methods to solve the maze
class MazeSolver(SearchProblem):
    # Initialize the class
    def __init__(self, board):
        self.board = board
        self.goal = (0, 0)

        #for y in range(len(self.board)):
        #    for x in range(len(self.board[y])):
        #        if self.board[y][x].lower() == "o":
        #            self.initial = (x, y)
        #        elif self.board[y][x].lower() == "x":
        #            self.goal = (x, y)
        #print(self.goal)
        self.initial = (1, 1)
        self.goal = (len(self.board)-2, len(self.board)-2)


        super(MazeSolver, self).__init__(initial_state=self.initial)

    # Define the method that takes actions
    # to arrive at the solution
    def actions(self, state):
        actions = []
        for action in COSTS.keys():
            newx, newy = self.result(state, action)
            if self.board[newy][newx] != "1":
                actions.append(action)

        return actions

    # Update the state based on the action
    def result(self, state, action):
        x, y = state

        if action.count("up"):
            y -= 1
        if action.count("down"):
            y += 1
        if action.count("left"):
            x -= 1
        if action.count("right"):
            x += 1

        new_state = (x, y)

        return new_state

    # Check if we have reached the goal
    def is_goal(self, state):
        return state == self.goal

    # Compute the cost of taking an action
    def cost(self, state, action, state2):
        return COSTS[action]

    # Heuristic that we use to arrive at the solution
    def heuristic(self, state):
        x, y = state
        gx, gy = self.goal

        return math.sqrt((x - gx)**2 + (y - gy)**2)


if __name__ == "__main__":
    f1 = open('complexity.txt', "w+")
    #f1 = open('size/complexity.txt', "w+")  # classic mazes with various sizes
    for j in range(100):
        for j2 in range(1,10):
            for j1 in range(1,10): # For classic maze
            #for j1 in range(5,105,5): # classic mazes with various sizes
                # Read maze from file
                f = open ('mazes/classic_maze_'+str(j)+'_'+str(j1)+'_'+str(j2)+'.txt', "r") # classic mazes
                #f = open ('size/mazes/classic_maze_'+str(j)+'_'+str(j1)+'.txt', "r") # classic mazes with various sizes
                MAP = f.read()
                MAP = MAP.replace(',', '')
                #print(MAP)
                f.close()
                # Convert map to a list
                MAP = [list(x) for x in MAP.split("\n") if x]

                # Define cost of moving around the map
                cost_regular = 1.0
                #   cost_diagonal = 1.7

                # Create the cost dictionary
                # The classic mazes have no diagonal movements
                COSTS = {
                    "up": cost_regular,
                    "down": cost_regular,
                    "left": cost_regular,
                    "right": cost_regular,
                }

                # Create maze solver object
                problem = MazeSolver(MAP)

                # Run the solver
                result = astar(problem, graph_search=True)

                # Extract the path
                try:
                    path = [x[1] for x in result.path()]
                except AttributeError:
                    path = ((0,0),(0,0))

                # Calculate Path-Complexity
                if path[1][0] == path[0][0]:
                    dir = 0
                else:
                    dir = 1
                turns = 0   # Number of turns
                d = 0       # Length of subpath
                C = 0       # Complexity
                for i in range(1,len(path)):
                    d = d + 1
                    if path[i][dir] != path[i-1][dir]:
                        C = C + 1/d
                        d = 0
                        turns = turns + 1
                        if dir == 0:
                            dir = 1
                        else:
                            dir = 0
                C = len(path) * C
                print(str(round(C,2)), file=f1)



                # Print the result
                #print()
                #for y in range(len(MAP)):
                #    for x in range(len(MAP[y])):
                #        if (x, y) == problem.initial:
                #            print('o', end='')
                #        elif (x, y) == problem.goal:
                #            print('x', end='')
                #        elif (x, y) in path:
                #            print('·', end='')
                #        else:
                #            print(MAP[y][x], end='')

                #    print()
    f1.close()





    # Define the map
#    MAP = 
"""
1111111111111111111111111111111
1000000000000000000000000000001
1011111110111111101111111111101
1000000010100000001000000000001
1011111110111011101110111110111
1010000000001010000000000010101
1010111011111011111011101010111
1010100010000000001000001010001
1010101110111111111111101110101
1010101000100000000000000000101
1010101010111011111110111110101
1000101000001000000010000000001
1011101110111010111111111011101
1010000010100000000000000010001
1010111011101110111111111010111
1000001010000010000000000010101
1011101010111111111111111010101
1000001010000000000000001010101
1010111011101111111111101010111
1010000000101000000000001000001
1010111110101111101111101010111
1010000000100000100010001010101
1010101111101111100010111011101
1010001000001000000010100000001
1110111011111111101110101111101
1000100010000000001000100000101
1111101011101111101111101111101
1000001000000000001000001000001
1000001111111111111011101000001
1000000000000000000000000000001
1111111111111111111111111111111
"""
