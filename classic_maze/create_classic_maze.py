#source: https://en.wikipedia.org/wiki/Maze_generation_algorithm#Python_code_example
#randomized prim's algorithm
#Edited by: Christian Parchem
#expand with entropy measuere and ziv-lempel-method
#fill boarders at the end, otherwise there is allways a path alongside the boarders

import numpy
from numpy.random import randint as rand
import matplotlib.pyplot as pyplot
import matplotlib.patches as mpatches
import math
from lempel_ziv_complexity import lempel_ziv_complexity #pip install lempel-ziv-complexity
from simpleai.search import SearchProblem, astar

def generate_maze(width=50, height=50, complexity=.75, density=.75):
    """Generate a maze using a maze generation algorithm."""
    # Only odd shapes
    shape = ((height // 2) * 2 + 1, (width // 2) * 2 + 1)
    # Adjust complexity and density relative to maze size
    complexity = int(complexity * (5 * (shape[0] + shape[1])))  # Number of components
    density    = int(density * ((shape[0] // 2) * (shape[1] // 2)))  # Size of components
    # Build actual maze
    Z = numpy.zeros(shape, dtype=int)

    # Make aisles
    for i in range(density):
        x, y = rand(0, shape[1] // 2) * 2, rand(0, shape[0] // 2) * 2  # Pick a random position
        Z[y, x] = 1
        for j in range(complexity):
            neighbours = []
            if x >= 1:             neighbours.append((y, x - 2))
            if x < shape[1] - 1:  neighbours.append((y, x + 2))
            if y >= 1:             neighbours.append((y - 2, x))
            if y < shape[0] - 1:  neighbours.append((y + 2, x))
            if len(neighbours):
                y_, x_ = neighbours[rand(0, len(neighbours))]
                if Z[y_, x_] == 0:
                    Z[y_, x_] = 1
                    Z[y_ + (y - y_) // 2, x_ + (x - x_) // 2] = 1
                    x, y = x_, y_
        # Fill borders
        Z[0, :] = Z[-1, :] = 1
        Z[:, 0] = Z[:, -1] = 1
    return Z

#Entropy H(x)=sum i=1 to n (p(x_i)*log_2(p(x_i)))
def entropy(env):
    global H
    for i in range(0,n-1):
        for j in range(0,n-1):
            if env[i,j]==0:
                x=int(4-env[i+1,j]-env[i-1,j]-env[i,j+1]-env[i,j-1])
                hx=0
                if x>1:
                    for k in range(1,x):
                        hx=hx-(1/x * math.log2(1/x))
                    H=H+hx
                    
def ziv_lempel(env):
    global C
    for i in range(1,n-1):
        s =''.join(map(str, env[i,1:n-1]))
        C=C+lempel_ziv_complexity(s)
        #print(C)




# Output ########################################################################

list = []
list.append([])
list.append([])
for k in range(0,100):
    f = open("output/output"+str(k)+".txt", "w+") 
    for i in range(1,10): 
        for j in range(1,10):
            H=0 #entropy
            C=0 #compression
            n=20; #size of the environment (nxn)
            Z=generate_maze(n, n, 0.1*i,0.1*j)
            entropy(Z)
            ziv_lempel(Z)
            Co=0.3*H+1.5*C

            #writing data to file
            print(str(round(i*0.1,2))+'\t'+str(round(j*0.1,2))+'\t'+str(round(H,2))+'\t'+str(C)+'\t'+str(round(Co,2)), file=f)
            if k==99:            
                #plotting maze
                pyplot.figure(figsize=(5, 5))
                pyplot.imshow(Z, cmap=pyplot.cm.binary, interpolation='nearest')
                pyplot.plot(1,1,'go',markersize=20)
                pyplot.plot(n-1,n-1,'ro',markersize=20)
                pyplot.axis('off')
                pyplot.tight_layout()
                pyplot.savefig('classic_maze_'+str(round(i,2))+'_'+str(round(j,2))+'.png')
                pyplot.close(fig=None)   
                #plotting diagram
                #p=i*0.1
                #pyplot.plot(p,Co,'g.',
                #            p,H,'r.',
                #            p,C,'b.')
                #pyplot.xlabel('Maze-Parameter(Complexity) ='+str(p))
                #pyplot.ylabel('')
                
            #writing maze to txt
            numpy.savetxt('mazes/classic_maze_'+str(k)+'_'+str(i)+'_'+str(j)+'.txt', Z, delimiter=',', fmt='%d')
        #red_patch = mpatches.Patch(color='red', label='Entropy')
        #blue_patch = mpatches.Patch(color='blue', label='Compression')
        #green_patch = mpatches.Patch(color='green', label='Complexity')
        #pyplot.legend(handles=[red_patch,blue_patch,green_patch],loc='upper center', bbox_to_anchor=(0, -0.07))
        #pyplot.title('Classic-maze with density='+str(round(0.1*j,1)))
        #pyplot.ylim(ymin=0)
        #pyplot.tight_layout()
        #pyplot.savefig("classic_maze/classic_maze_density="+str(j)+".png")
        #pyplot.close(fig=None)
    f.close()
