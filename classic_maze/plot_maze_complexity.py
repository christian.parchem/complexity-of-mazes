# Statistical calculation of maze-parameters
# Christian Parchem
# date: 10.02.2020


import statistics
from scipy import stats

import matplotlib.pyplot as pyplot
import matplotlib.patches as mpatches
pyplot.style.use('seaborn-whitegrid')

###### Classic Maze ##########################################

# print data from output-files to dictionary
dic = dict()
for i in range(0,100):
    array = []
    with open("output/output"+str(i)+".txt", "r") as f:
        for line in f.readlines():
            line.split(' ')
            array.append([float(x) for x in line.split()])
    dic[i] = array
    f.close()
# print data from complexity file to arraypc
for i in range(0,100):
    arraypc = []
    with open("complexity.txt", "r") as f:
        for line in f.readlines():
            arraypc.append(float(line))
    f.close()
for i in range(9):
    # plot data from dictionary
    fig = pyplot.figure(figsize=(10, 7))
    pyplot.rcParams.update({'font.size': 18})
    ax = fig.add_subplot(111)
    ax.tick_params(axis='x', labelsize=20)
    ax.tick_params(axis='y', labelsize=20)
    #for i in range(0,81,10):
    for k in range(9):
        co = [] #complexity
        for j in range(0,100):
            co.append(dic[j][i*9+k][4])
        mco = statistics.mean(co)
        vco = statistics.variance(co, mco)
        sdevco = statistics.stdev(co)
        meco = statistics.median(co)

        pc = [] #path-complexity
        for j in range(0,100):
            pc.append(arraypc[j*9*9+i*9+k])
        mpc = statistics.mean(pc)
        vpc = statistics.variance(pc, mpc)
        sdevpc = statistics.stdev(pc)
        mepc = statistics.median(pc)

        h = [] #entropy
        for j in range(0,100):
            h.append(dic[j][i*9+k][2])
        mh = statistics.mean(h)
        vh = statistics.variance(h, mh)
        sdevh = statistics.stdev(h)
        meh = statistics.median(h)

        c = [] #compression
        for j in range(0,100):
            c.append(dic[j][i*9+k][3])
        mc = statistics.mean(c)
        vc = statistics.variance(c, mc)
        sdevc = statistics.stdev(c)
        mec = statistics.median(c)

        #plotting diagram
        p=0.1*(k+1)
        pyplot.plot(p,mh,'go',
                    p,mco,'ro',
                    p,mc,'bo',
                    p,mpc,'mo')
        #Create an error bar
        pyplot.rcParams.update({'errorbar.capsize': 4})
        pyplot.errorbar(p, mh, yerr=sdevh, fmt='g.')
        pyplot.errorbar(p, mco, yerr=sdevco, fmt='r.')
        pyplot.errorbar(p, mc, yerr=sdevc, fmt='b.')
        pyplot.errorbar(p, mpc, yerr=sdevpc, fmt='m.')


        pyplot.xlabel('Maze-Parameter (Density)', fontsize=20)
        pyplot.ylabel('Mean', fontsize=20)
        pyplot.ylim(ymin=0)



    red_patch = mpatches.Patch(color='red', label='Complexity')
    green_patch = mpatches.Patch(color='green', label='Entropy')
    blue_patch = mpatches.Patch(color='blue', label='Compression')
    magenta_patch = mpatches.Patch(color='magenta', label='Path-Complexity')
    pyplot.legend(handles=[red_patch,green_patch,blue_patch,magenta_patch],loc='upper center', bbox_to_anchor=(0, -0.07))
    pyplot.title('Classic-maze with complexity parameter = '+str(round(0.1*i+0.1,1)), fontsize=20)
    pyplot.tight_layout()
    pyplot.savefig("classic_maze_c"+str(i)+".png")
    pyplot.show()
    pyplot.close(fig=None)

