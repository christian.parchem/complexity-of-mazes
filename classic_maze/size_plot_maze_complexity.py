# Statistical evaluation of the mazes
# Christian Parchem
# date: 10.02.2020


import statistics
from scipy import stats

import matplotlib.pyplot as pyplot
import matplotlib.patches as mpatches
pyplot.style.use('seaborn-whitegrid')

###### Classic Maze ##########################################

# print data from output-files to dictionary
dic = dict()
for i in range(0,100):
    array = []
    with open("size/output/output"+str(i)+".txt", "r") as f:
        for line in f.readlines():
            line.split(' ')
            array.append([float(x) for x in line.split()])
    dic[i] = array
    f.close()
# print data from complexity file to arraypc
arraypc = []
with open("size/complexity.txt", "r") as f:
    for line in f.readlines():
        arraypc.append(float(line))
f.close()
# plot data from dictionary
fig = pyplot.figure(figsize=(10, 7))
pyplot.rcParams.update({'font.size': 18})

for i in range(20):
    co = [] #complexity
    for j in range(0,100):
        co.append(dic[j][i][4])
    mco = statistics.mean(co)
    vco = statistics.variance(co, mco)
    sdevco = statistics.stdev(co)
    meco = statistics.median(co)

    pc = [] #path-complexity
    for j in range(0,100):
        pc.append(arraypc[j*20+i])
    mpc = statistics.mean(pc)
    vpc = statistics.variance(pc, mpc)
    sdevpc = statistics.stdev(pc)
    mepc = statistics.median(pc)
    
    h = [] #entropy
    for j in range(0,100):
        h.append(dic[j][i][2])
    mh = statistics.mean(h)
    vh = statistics.variance(h, mh)
    sdevh = statistics.stdev(h)
    meh = statistics.median(h)
    
    c = [] #compression
    for j in range(0,100):
        c.append(dic[j][i][3])
    mc = statistics.mean(c)
    vc = statistics.variance(c, mc)
    sdevc = statistics.stdev(c)
    mec = statistics.median(c)
    
    #plotting diagram
    ax = fig.add_subplot(111)
    ax.tick_params(axis='x', labelsize=20)
    ax.tick_params(axis='y', labelsize=20)
    p=5*(i+1)
    pyplot.plot(p,mh,'go',
                p,mco,'ro',
                p,mc,'bo',
                p,mpc,'mo')
    #Create an error bar
    pyplot.rcParams.update({'errorbar.capsize': 4})
    pyplot.errorbar(p, mh, yerr=sdevh, fmt='g.')
    pyplot.errorbar(p, mco, yerr=sdevco, fmt='r.')
    pyplot.errorbar(p, mc, yerr=sdevc, fmt='b.')
    pyplot.errorbar(p, mpc, yerr=sdevpc, fmt='m.')
    
    
pyplot.xlabel('Size', fontsize=20)
pyplot.ylabel('Mean', fontsize=20)
pyplot.ylim(ymin=0)
    


red_patch = mpatches.Patch(color='red', label='Complexity')
green_patch = mpatches.Patch(color='green', label='Entropy')
blue_patch = mpatches.Patch(color='blue', label='Compression')
magenta_patch = mpatches.Patch(color='magenta', label='Path-Complexity')
pyplot.legend(fontsize=22)
pyplot.legend(handles=[red_patch,green_patch,blue_patch,magenta_patch],loc='upper center', bbox_to_anchor=(0, -0.07))
pyplot.title('Classic-maze', fontsize=20)
pyplot.tight_layout()
pyplot.savefig("size/classic_maze.png")
pyplot.show()
pyplot.close(fig=None)
