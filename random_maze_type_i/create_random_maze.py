# Maze-generator for randomized mazes
# Author of maze generator: Tomas Kulvicius
# Edited by: Christian Parchem


import numpy as np
import random
import matplotlib.pyplot as pyplot
import math
from lempel_ziv_complexity import lempel_ziv_complexity #pip install lempel-ziv-complexity
import matplotlib.patches as mpatches

n=20; #size of the environment (nxn)    

#start position
sx=1;
sy=1;

#goal position
gx=n-2;
gy=n-2;

env_type = 1; #three types are possible: 1, 2, and 3


        
def type_I(env,v,n):     
    #generate environment with obstacles "maze - type I"
    #note that it may happen that there is no possible way from start to end
    for i in range(0,n):
        for j in range(0,n):
            if random.uniform(0,1)>0.05*v:
                env[i,j]=1
    env[sx,sy]=0
    env[gx,gy]=0
        
def type_II(env,v,n):
    #generate environment with obstacles "maze - type II" 
    for i in range(3,n-2,2):
        for j in range(1,n):
            if random.uniform(0,1)>0.05*v:
                env[i,j]=1
        if sum(env[i,2:n-1])==n-2:
            a=random.permutation(n-2);
            env[i,a[1]+1]=0;
    for i in range(2,n-1,2):
        for j in range(1,n):
            if random.uniform(0,1)>0.95:
                env[i,j]=1
    env[sx,sy]=0;
    env[gx,gy]=0;
        
def type_III(env,v,n):
        #generate environment with obstacles "maze - type III"
        offset=1
        env[offset:n-offset,offset:n-offset]=0

        if random.uniform(0,1)>0.05*v+0.1:
            for i in range(offset,n-offset):
                for j in range(offset+1,n-offset):
                    if env[i-1,j-1]==1 and env[i-1,j]==0 and env[i-1,j+1]==0: 
                        env[i,j]=0
                    elif env[i-1,j-1]==0 and env[i-1,j]==0 and env[i-1,j+1]==1: 
                        env[i,j]=0
                    elif env[i-1,j-1]==1 and env[i-1,j]==0 and env[i-1,j+1]==1: 
                        env[i,j]=0    
                    elif random.uniform(0,1)>0.05*v: #0.4
                        env[i,j]=1
                    if (i==sx and j==sy) or (i==gx and j==gy):
                        env[i,j]=0
        else:
            for j in range(offset+1,n-offset):
                for i in range(offset+1,n-offset):
                    if env[i-1,j-1]==1 and env[i,j-1]==0 and env[i+1,j-1]==0: 
                        env[i,j]=0
                    elif env[i-1,j-1]==0 and env[i,j-1]==0 and env[i+1,j-1]==1: 
                        env[i,j]=0
                    elif env[i-1,j-1]==1 and env[i,j-1]==0 and env[i+1,j-1]==1: 
                        env[i,j]=0
                    elif random.uniform(0,1)>0.05*v: #0.4
                        env[i,j]=1
                    if (i==sx and j==sy) or (i==gx and j==gy):
                        env[i,j]=0

def switch_env_type(env_type,v,n):
    switcher = {
        1: lambda: type_I(env,v,n),
        2: lambda: type_II(env,v,n),
        3: lambda: type_III(env,v,n)
    }
    return switcher.get(env_type)()

def entropy(env):
    global H
    for i in range(0,n-1):
        for j in range(0,n-1):
            if env[i,j]==0:
                x=int(4-env[i+1,j]-env[i-1,j]-env[i,j+1]-env[i,j-1])
                hx=0
                if x>1:
                    for k in range(1,x):
                        hx=hx-(1/x * math.log2(1/x))
                    H=H+hx

#ziv-lempel-algorithm                    
def ziv_lempel(env):
    global C
    for i in range(1,n-1):
        s =''.join(map(str, env[i,1:n-1]))
        C=C+lempel_ziv_complexity(s)



#printing data
for k in range(0,100): #0-100
    f = open("output/output"+str(k)+".txt", "w+") 
    for i in range(1,20): # 1-20
        #prepare grid
        env=np.ones([n,n])
        env=np.asarray(env, dtype=int)
        env[1:n-1,1:n-1]=0

        H=0 #entropy
        C=0 #compression    
        #v=15 #maze-parameter

        switch_env_type(env_type,i,n)
        entropy(env)
        ziv_lempel(env)
        Co=0.3*H+1.5*C #Complexity

        #writing to output.txt
        print(str(round(i*0.05,2))+'\t'+str(round(H,2))+'\t'+str(C)+'\t'+str(round(Co,2)), file=f)
        #writing maze to txt
        np.savetxt('mazes/random_maze_'+str(k)+'_'+str(i)+'.txt', env, delimiter=',', fmt='%d')
        if k==99: 
            #plotting mazes
            pyplot.figure(figsize=(5, 5))
            pyplot.imshow(env, cmap=pyplot.cm.binary, interpolation='nearest')
            pyplot.plot(sy,sx,'go',markersize=10)
            pyplot.plot(gy,gx,'ro',markersize=10)
            pyplot.axis('off')
            pyplot.tight_layout()
            pyplot.savefig("random_maze_"+str(i)+".png")
            pyplot.close(fig=None)
    f.close()
